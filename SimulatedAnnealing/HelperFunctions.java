import java.util.Random; //So we can generate a random number  

//This class will have functions that calculate distance and the acceptance probability 
public class HelperFunctions{

	//Calculates the eculidean distance between two cities and returns it 
    public double EuclideanDistance(City city1, City city2){

       double xDist = Math.abs(city1.GetX() - city2.GetX());//Find distance between cities for the x and y coordinates 
       double yDist = Math.abs(city1.GetY() - city2.GetY()); 

       double squareXDist = Math.pow(xDist,2);//Calculate using standard euclidean distance 
       double squareYDist = Math.pow(yDist,2); 

       double distance = Math.sqrt(squareXDist + squareYDist);
       return distance; 
    }
 
    //Function returns a double between 0.0 and 1.0
    public double RandomDouble(){
    	Random rd = new Random(); // creating Random object
        return rd.nextDouble(); // generates a random double value between 0.0 & 1.0
    }

    /*Min is inclusive and max is exclusive 
    Function returns a random int between the specified range 
    */
    public int RandomInt(int min, int max){
    	Random rd = new Random(); 

    	int rand = (int)(rd.nextDouble() * (max-min)+min); //generates a random double 
    	return rand; //typecast it to int 
    }
    

    /* Passed the current distance (currentEnergy)& the new distance(newEnergy) as well as the temperature 
      This function calculates, what's called the acceptance probability with the use of a simple equation. 
    */ 
    public double AcceptanceProbability(double currentEnergy, double newEnergy, double temp){
         if(currentEnergy > newEnergy ) return 1; //If there is a better solution accept it 

         return Math.exp((currentEnergy - newEnergy)/ temp); //If not calculate probability, which determines worth of acceptance 

    }

}