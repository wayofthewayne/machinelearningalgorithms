import java.util.Collections;
import java.io.*; //For file reading 

//Here the main simulated annealing algorithm will be implemented. 
public class SimulatedAnnealing{
	public static void main (String [] args){

        Tour currentSol = new Tour(); 

        File filename = new File("InputNodes.txt"); //creating file instance 
        ReadFile(currentSol, filename); //we process the file 


        currentSol.CreateRandomTour(); //Creates our candidate solution

        double temp = 100000; //Setting the initial parameters 
        double coolingRate = 0.997; 

        HelperFunctions help = new HelperFunctions(); //so we may use the helper functions 
        
        Tour best = new Tour(currentSol.GetTour()); 
        //We want to be able to keep track of the best solution 
        //Initially we assume the current random solution is the best 

        String initialTourList = Display(currentSol);//Display initial tour and length 
        System.out.println("Initial solution: " + initialTourList); 
        System.out.println("Initial Solution Length: " + currentSol.GetTourDistance()); 

        while(temp > 0.03){ //Loop until the system has been cooled enough 

        	Tour newSol = new Tour(currentSol.GetTour()); // Create a neighbour tour 

        	// Get random positions in the tour
            int randomPos1 = help.RandomInt(0 , newSol.TourSize());
            int randomPos2 = help.RandomInt(0 , newSol.TourSize());

            while(randomPos1 == randomPos2){ //making sure the two positions are different 
               randomPos2 = help.RandomInt(0 , newSol.TourSize());
            }

            Collections.swap(newSol.GetTour(),randomPos1,randomPos2);//swapping the two cities at these positions 

            double currentDist = currentSol.GetTourDistance(); //Get distances for both solutions 
            double neighbourDist = newSol.GetTourDistance(); 

            double rand = help.RandomDouble(); 
            if (help.AcceptanceProbability(currentDist, neighbourDist, temp) > rand) { //Decide if we should accept new sol 
                currentSol = new Tour(newSol.GetTour());
            }

            //Keeping record of best solution so far 
            if(currentSol.GetTourDistance() < best.GetTourDistance()){
            	best = new Tour(currentSol.GetTour()); 
            }
             
            temp *= coolingRate; //cool down system

        }

        String bestTourList = Display(best); //Display best tour and length 
        System.out.println("Best Solution: " + bestTourList);  
        System.out.println("Final Solution Length: " + best.GetTourDistance());
	}
    
    //Function is passed the tour and it returns a string of containing all the city names in the tour 
	public static String Display(Tour tour){

        String tourList = tour.GetCity(0).cityName;  //holds the city names of the tour 
        for (int i = 1; i < tour.TourSize(); i++) { 
            tourList += " -> " + tour.GetCity(i).cityName; //loops through the tour and adds the names 
        }
        return tourList; 
	}

   //Function opens the file and reads all the input, which are then stored in the tour.
	public static void ReadFile(Tour tour, File filename){ //Function will open the file and start to process line by line 
          BufferedReader reader; 

         try{
            
           reader = new BufferedReader (new FileReader(filename));
           String line = reader.readLine(); //line is stored in string           

           while(line != null){ //Till all the file is processed 
               
              String[] tokens = line.split(" ",-1); //parse the string by space 

              City node = new City(tokens[0],Double.valueOf(tokens[1]),Double.valueOf(tokens[2])); //calling city constuctor to set the city 
              tour.citiesToVisit.add(node); //add it to the tour 

              line = reader.readLine(); //move to next line
           }

           reader.close(); //close file

         } catch (IOException e){ // Output errors with opening file 
           e.printStackTrace ();
         }
 	}



}
