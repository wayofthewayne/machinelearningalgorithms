import java.util.Collections; //Constains a method which allows us to shuffle 
import java.util.ArrayList; // so we can make use of the array list object/ data structure  
import java.lang.Math; //To use the math functions like square root and power and abs 


//This class will hold a tour(explained below)
//A tour will be a candidate solution 
public class Tour{

	private ArrayList<City> tour = new ArrayList<City>();
	//This will hold a list of cities. 
	//Such list would visit all cities and return to its starting place. 
	//This is called a tour 


	public ArrayList<City> citiesToVisit = new ArrayList<City>(); //(This list will help keep track of cities in a tour)
	//It will contain, as the name indicates, all the cities we need to visit;  

	public double distance = 0; //Initial distance will be set to 0; 
     
    public Tour(){//Start an empty tour (empty constructor)
        for(int i=0; i<citiesToVisit.size(); i++){
        	tour.add(null); 
        }
    }
    
    //Function causes unchecked errors 
    public Tour(ArrayList<City> someTour){ //Parameterised constructor 
        tour = (ArrayList<City>) someTour.clone(); //Copy contents of someTour to tour
    }

    public City GetCity(int index){ //CITY getters and setters 
       return tour.get(index); 
    }

    public void SetCity(int index, City city){ //Adds a city to the tour 
    	tour.add(index, city); 
    	distance = 0; // Tour is changed so we reset distance 
    }

    public int TourSize(){ //returns the size of the tour 
    	return tour.size(); 
    }

    public ArrayList<City> GetTour(){ //Returns the tour 
    	return tour; 
    }
    
    public void CreateRandomTour(){ //This will generate a candidate solution 

       for(int i=0; i<citiesToVisit.size(); i++){ //Loop through the cities to be visited 
           SetCity(i, citiesToVisit.get(i)); //Set the tour 
       }

       Collections.shuffle(tour); //This randomly shuffles a collection of objects
       //This has the effect of creating a random tour 
    }

    //function finds the total distance of this tour, and returns it. 
    public double GetTourDistance(){
    	if(distance == 0){

    		double tourLength = 0; //will hold the tour length 
    		HelperFunctions findDistance = new HelperFunctions(); 

    		for(int i=0; i<TourSize(); i++){ //loop through all the tour 

    			City startingPoint = GetCity(i); //get the starting city
    			City arrival; //To store next city to visit 

    			if(i+1 < TourSize() ){ //check to see if we are at end of tour 
                    arrival = GetCity(i+1); //Go to next city
    			}else{
    				arrival = GetCity(0); //Go to beginning of tour (part of tsp conditions)
    			}

                 tourLength += findDistance.EuclideanDistance(startingPoint,arrival); //Find the distance between the two cities 
    		}
            
            distance = tourLength; //set the distance
    	}

    	return distance;
    }



}