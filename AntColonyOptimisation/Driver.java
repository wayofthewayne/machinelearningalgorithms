import java.io.*;

public class Driver{
	public static void main(String [] args){

		File filename = new File("InputNodes.txt"); //storeing file name 
		City holdTour = new City(); 
		ReadFile(holdTour, filename); //processing the file 

		double [][] distanceMatrix = new double[holdTour.citiesToVisit.size()][holdTour.citiesToVisit.size()]; //initialising the matrix 

		CalculateDistanceMatrix(distanceMatrix, holdTour); //setting costMatrix 

		AntColonyOptimisation antCol = new AntColonyOptimisation(holdTour.citiesToVisit.size(), distanceMatrix); //initiliase the optimisation 
		antCol.RunOptimisation(); //run it 
	}

	//Function opens the file and reads all the input, which are then stored in the tour.
	public static void ReadFile(City tour, File filename){ //Function will open the file and start to process line by line 
          BufferedReader reader; 

         try{
            
           reader = new BufferedReader (new FileReader(filename));
           String line = reader.readLine(); //line is stored in string           

           while(line != null){ //Till all the file is processed 
               
              String[] tokens = line.split(" ",-1); //parse the string by space 

              City node = new City(tokens[0],Double.valueOf(tokens[1]),Double.valueOf(tokens[2])); //calling city constuctor to set the city 
              tour.citiesToVisit.add(node); //add the city into the cities to be visited 

              line = reader.readLine(); //move to next line
           }

           reader.close(); //close file

         } catch (IOException e){ // Output errors with opening file 
           e.printStackTrace ();
         }
 	}

 	//Calculates the eculidean distance between two cities and returns it 
    public static double EuclideanDistance(City city1, City city2){

       double xDist = Math.abs(city1.GetX() - city2.GetX());//Find distance between cities for the x and y coordinates 
       double yDist = Math.abs(city1.GetY() - city2.GetY()); 

       double squareXDist = Math.pow(xDist,2);//Calculate using standard euclidean distance 
       double squareYDist = Math.pow(yDist,2); 

       double distance = Math.sqrt(squareXDist + squareYDist);
       return distance; 
    }


    //Will store all the distances for a in matrix. It is passed the matrix to hold the values, and a city containing all the cities to be visied 
    public static void CalculateDistanceMatrix(double[][] distanceMatrix, City holdTour){
        for(int i=0; i<holdTour.citiesToVisit.size(); i++){
        	for(int j=0; j<holdTour.citiesToVisit.size(); j++){
                   distanceMatrix[i][j] = EuclideanDistance(holdTour.citiesToVisit.get(i), holdTour.citiesToVisit.get(j)); //function call to compute the distance 
                   //Note this has an effect of shifting 1-n cities to 0 to n-1 (i.e index of cities is shifted back by one)
                   //The rest of the program will also work with this shift 
                   //Will be catered for in printing by shifting back 
        	}
        }
    }
}