public class Ant{//class to model an atrificial ant 

	private int pathSize; //wil hold the number of cities to be visited 
	private int path[]; //will contain the cities 
	private boolean visited[]; //will keep track of what cities were visited 


    //Sets ant variables based on the size of the tour
	public Ant(int tourSize){//Ant constructor
		this.pathSize = tourSize; 
		this.path = new int[tourSize];
		this.visited = new boolean[tourSize]; 
	}

	public int GetPath(int index){ //retreives a city from a path of an ant 
         return path[index]; 
	}

	public int[] GetPathAll(){ //retreives the whole path an ant has travalled from 
		return this.path; 
	}


    //Function lets an ant visit a city. City here will be a node no. (i.e an int)
	public void VisitCity(int currentIndex, int city){ //We represent city by an integer here  
		path[currentIndex+1] = city; //set path index and respective city visited 
		visited[city] = true; //setting visited city to true 
	}

	public boolean GetVisited(int index){//Lets us check if a specific city was visited 
		return visited[index];
	}

	public void Clear(){ //Clears all the cities, setting them as not visited 
		for(int i=0; i<pathSize; i++){
			visited[i] = false; 
		}
	}


    /*Function returns the total length of a path 
    *costMatrix[i][j] will contain distance from city i to city j 
    */
	public double GetPathLength(double costMatrix[][]){
		double distance = costMatrix[path[pathSize-1]][path[0]]; //finding distance from last city to beginning city and getting distance (i.e city n to 0)

		for(int i=0; i<pathSize-1; i++){
			distance += costMatrix[path[i]][path[i+1]]; //finding distance from city 0 to the next city for n cities
		}

		return distance; //returning the distance 
	}
}