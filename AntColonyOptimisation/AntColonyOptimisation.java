import java.io.*; 
import java.util.ArrayList;
import java.util.Random; 
import java.lang.*; 

public class AntColonyOptimisation{ //This class will contain all the methods related to the ACO and how they intricately work together 
	//It will also contain the parameters related to the ACO 

	//Initial parameters (FIXED)
	
	public double initialPhermone = 1.0; //will hold the initial amount of phermone and will be used to clear paths 
	public double alpha = 1; //(phermone potency)
	public double beta = 3; //controls distance priority 
	public double evaporationRate = 0.5; //rate of evaporation of the phermone per iteration 
	public double phermoneTrail = 350; //total phermone left on a trial by the ants 
	public double antFactor = 0.8; //no of ants per city 
	public double randomFactor = 0.01; //introduces a factor of randomness 

	public int noOfIteration = 1000; //The number of iterations which will be run 
	//The amount of moves the ants have 

	//variables based on the the amount of cities being considered 

	public int noOfCities; //stores the number of cities 
	public int noOfAnts;  //number of ants in total 
	public double costMatrix[][]; //will hold the distances between nodes 
	public double phermoneMatrix[][]; //holds the phermone in the paths 
	public ArrayList<Ant> ants = new ArrayList<Ant>(); //a list of ants 
    
    //other needed variables 
	public Random random = new Random(); //instances of random class 
	public double probability[]; //stores the probabilities of where the ant should go 
    public int currentIndex; //holds the current index 
    
    //variables related to the best solution 
    public int[] bestTour; //holds the best tour 
    public double bestTourLength; //holds the best tour length 


    /*We pass the number of cities being considered and the distanceMatrix 
    */ 
    public AntColonyOptimisation(int numberOfCities, double[][] distanceMatrix){//ACO constructor 

      costMatrix = distanceMatrix; //setting the cost matrix 
      noOfCities = costMatrix.length; //We initialse rest of parameters based on the number of cities 
      noOfAnts = (int) (noOfCities * antFactor); 
      phermoneMatrix = new double[noOfCities][noOfCities]; 
      probability = new double[noOfCities];

      for(int i=0; i<noOfAnts; i++){
      	ants.add(i,new Ant(noOfCities)); //we add the ants to the list 
      }

    }
    
    //Sets all ants into a random city; 
    public void SetupAnts(){

    	for(int j=0; j<ants.size(); j++){ //for all the ants 
    		Ant ant = ants.get(j); //retrieve the ant 
    		ant.Clear(); //set all the visited cities to false 
    		ant.VisitCity(-1,random.nextInt(noOfCities)); //visit a random city 
    	}

    	currentIndex =0; //set the currentIndex 
    }

    public void ClearPaths(){ //removes phermone from the path 

    	for(int i=0; i<noOfCities; i++){
    		for(int j=0; j<noOfCities; j++){
    			phermoneMatrix[i][j] = initialPhermone; //setting all the phermone to 1
    		}
    	}
    }
    

    //For all the cities, each ant is made to move to the next city 
    public void MoveAnts(){
    	for(int i =currentIndex; i<noOfCities-1; i++){ //from the current index to the city before the last (avoids out of bounds index in VisitCity)

           for(int j=0; j<ants.size(); j++){ //for all the ants
           	  Ant ant = ants.get(j); //get the ant

           	  ant.VisitCity(currentIndex, FindNextCity(ant)); //make ant visit another city 
           }
           currentIndex++; //update index in the path 
    	}
    }


    //An ant is passed, and for that ant a city returned 
    public int FindNextCity(Ant ant){
    	int randCity = random.nextInt(noOfCities);

    	if(random.nextDouble() < randomFactor){ //Check to see if any should visit a random city

    		for(int i=0; i<noOfCities; i++){ //for the number of cities 

    		    if(i==randCity && ant.GetVisited(i) == false){ //finds the random city and see if it is visited 
                      return i; //return city 
    		    } 
    		}
    	}
        
        CalculateProbabilities(ant); //Ant finds out where he should go 
        double rand = random.nextDouble(); //generate random number between 0 and 1 
        double totalProb=0; 

        for(int i=0; i<noOfCities; i++){

           totalProb += probability[i]; 
           if(totalProb >= rand){ //once the sum of probabilities has a greater value than the random number 
           	    return i; //we return the city
           }

        }

        throw new RuntimeException("There are no other cities"); //needed so to avoid missing return statement error 

    }
    

    //Helps set the probabilities for possible paths 
    public void CalculateProbabilities(Ant ant){
         int pos = ant.GetPath(currentIndex); //find out where ant is 
         double phermone = 0.0; 

         for(int i=0; i<noOfCities; i++){
            if(ant.GetVisited(i) == false){
            	phermone += Math.pow(phermoneMatrix[pos][i],alpha) * Math.pow(1.0/costMatrix[pos][i], beta); //phermone from unvisited path is 'sensed' 
            }
         }

         for(int i=0; i<noOfCities; i++){
         	if(ant.GetVisited(i) == true){
         		probability[i]=0.0; //ant does not have much chance of visiting city already visited 
         	}else{
         		probability[i] = ( Math.pow(phermoneMatrix[pos][i], alpha) * Math.pow(1.0 / costMatrix[pos][i], beta) ) / phermone; 
         		//using equation probability is calculated in relation to the phermone and cost matrix 
         	}
         }
    }
    

    //Function keeps track of best tour 
    public void UpdateBest(){
    	if(bestTour == null){ //as initial solution set ant 0 path 
    		bestTour = ants.get(0).GetPathAll(); 
    		bestTourLength = ants.get(0).GetPathLength(costMatrix);
    	}


    	for(int i=0; i<ants.size(); i++){//go through all ants  
          if(ants.get(i).GetPathLength(costMatrix) < bestTourLength){ //check for better solution 
              bestTour = ants.get(i).GetPathAll().clone();  //copy path for ant as best path 
              bestTourLength = ants.get(i).GetPathLength(costMatrix); //set length for the best path 
          }
    	}
    }
    
    //Updates the phermone across the routes 
    public void UpdatePaths(){

    	for(int i=0; i<noOfCities; i++){ 
    		for(int j=0; j<noOfCities; j++){
    			phermoneMatrix[i][j] *= evaporationRate; //update the phermone across all the paths using phermone evaporation rate; 
    		}
    	}

    	for(int i=0; i<ants.size(); i++){ //for each ant 
    		double contribute = phermoneTrail / ants.get(i).GetPathLength(costMatrix); //determine contribution factor 

    		for(int j=0; j<noOfCities-1; j++){ //for all the cities 
    			phermoneMatrix[ants.get(i).GetPath(j)][ants.get(i).GetPath(j+1)] += contribute; 
    		} //set the contribution rate across all the routes 

    		phermoneMatrix[ants.get(i).GetPath(noOfCities-1)][ants.get(i).GetPath(0)] += contribute;
    		//This has an effect of how influential phermone is 
    	}
    }


    //Contains the main algorithm for the ACO 
    public void Algo(){

    	SetupAnts();//we setup ants and clear all the paths 
    	ClearPaths(); 

    	for(int i=0; i<noOfIteration; i++){ //for all the iterations 
    		MoveAnts(); //we move the ants 
    		UpdatePaths(); //update all the paths  
    		UpdateBest(); //update the best path 
    	}

    	System.out.println("Best tour length: " + bestTourLength); //then print best tour 
    	String bestTourOrder = Display(bestTour); 
    	System.out.println("Best tour order: " + bestTourOrder); 
    }

    //Function is passed the best tour containing all the cities and it stores them in a string  
	public static String Display(int[] bestTour){

        String tourList = String.valueOf(bestTour[0]+1);  //note we add the +1 to cater for the previous shift in the beginning 
        for (int i = 1; i<bestTour.length; i++) { 
            tourList += " -> " + String.valueOf(bestTour[i]+1); //loops through the tour and adds the names 
        }
        return tourList; 
	}
    

    //Run the optimisation
    public void RunOptimisation(){
    		Algo(); 
    }    
}  
