//Here we define a city as a point on the cartesian plane, along with a designated name 
public class City{

	private double x; //Will serve as x,y coordinates in a cartesian plane 
	private double y; 
	public String cityName; //Note this will store either the city name or node No;

	City(){
	}


	City(String cityName, double xCoor, double yCoor){ //Constuctor creates a city by setting it's x and y coordinates and the city name 
		x = xCoor; 
		y = yCoor; 
        this.cityName = cityName; 
	}

	//Below are the setters and getters for x and  y 

	public double GetX(){
		return x; 
	}

	public void SetX(double x){
		this.x=x;
	}

	public double GetY(){
         return y; 
	}

	public void SetY(double y){
		this.y=y; 
	}


}